Juan Sebastian Velez Posada
-------------

1 – Codificar un método que devuelva el segundo mayor número de una lista. Por ejemplo, dada la lista
de números 5, 10, 4, 9, 16, debería retornar 10.

:   Go to *sebas.tian.ejerciciosjava.Main* class and execute *runSecondHighest()* method

2 - Factorials can be defined as follows:

:   Go to *sebas.tian.ejerciciosjava.Main* class and execute *runFactorial()* method

3 – ¿Hay algún problema con este código? 
:   
```java
String query = "SELECT * FROM users WHERE userid ='"+ userid + "'" + " AND password='"+ password + "'";
PreparedStatement stmt = connection.prepareStatement(query);
ResultSet rs = stmt.executeQuery();
```

As it is the code will work, but there is not use of the *? statements* that 
*PreparedStatement* class offers us, this statements will go where *userid* or 
*password* is, they will prevent SQL injection for example.


4 - What is the difference between the following statements
:   
   a) if (a.equals(b)) {…};
   b) if (a == b) {…};
   Is one of them better than the other?

I can not say one is better that the other because they are use for different purposes.
Option a will do what equals method on class from object a is supposed to do,
default behaviour is to compare the values between two objects, it comes from
Object class and this method can be overridden.
Option b is a reference comparison, for example both objects point to the same memory location.

5 - Which of the following code fragments you think is better? Explain why in one sentence
:   
   a) public void processList(List theList) {…}
   b) public void processList(ArrayList theList) {…}

I would go with the first choice, because the parameter is an interface and 
the second option have as a parameter an actual class.
With this the method *processList* wont be tie to an specific class, 
and it will be able to work with all classes that implement List interface.


6 – Write a Singleton implementation.

:   Go to *sebas.tian.ejerciciosjava.Main* class and execute *runSingleton()* method