package sebas.tian.ejerciciosjava;

/**
 *
 * @author jvelez
 */
public class Singleton {
    
    private static Singleton instance;
    
    private Singleton() {
        // only to prevent other classes to instantiate 
    }
    
    public static Singleton getInstance() {
        
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
    
    public void printSomething(){
        System.out.println("Hello im the singleton instance this is my hash " + this.hashCode());
    }
}
