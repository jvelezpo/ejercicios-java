package sebas.tian.ejerciciosjava;

import java.util.Arrays;

/**
 *
 * @author jvelez
 */
public class Main {
    
    
    public static void main(String[] args) {
        
        // 1 Exercise
        runSecondHighest();
        // 2 Exercise
        runFactorial();
        // 6 Exersice
        runSingleton();
    }
    
    
    public static void runSecondHighest() {
        
        int[] testCase = {5, 10, 4, 9, 16};
        
        SecondHighest secondHighest = new SecondHighest();
        
        int result = secondHighest.getSecondHighest(testCase);
        
        System.out.println("The second highest between " + Arrays.toString(testCase)+ " is: " + result);
    }
    
    public static void runFactorial() {
        
        int getMyFactorial = 4;
        
        Factorial factorial = new Factorial();
        
        int result = factorial.factorial(getMyFactorial);
        
        System.out.println("The of " + getMyFactorial + " is: " + result);
    }
    
    public static void runSingleton() {
        
        Singleton singleton = Singleton.getInstance();
        
        singleton.printSomething();
        
        //Get another instance just for fun
        //singleton and singleton 2 should print the same
        
        Singleton singleton2 = Singleton.getInstance();
        
        singleton2.printSomething();
    }
}
