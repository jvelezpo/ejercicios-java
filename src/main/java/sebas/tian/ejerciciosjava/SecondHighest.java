package sebas.tian.ejerciciosjava;

/**
 *
 * @author jvelez
 */
public class SecondHighest {

    /**
     * Loops through all the array looking for the second highest number big
     * O(n) time
     *
     * @param array
     * @return
     */
    public int getSecondHighest(int[] array) {

        int theHighest = Integer.MIN_VALUE + 1;
        int secondHighest = Integer.MIN_VALUE;

        for (int i : array) {

            if (i > theHighest) {
                secondHighest = theHighest;
                theHighest = i;
            } else if (i > secondHighest && i != theHighest) {
                secondHighest = i;
            }
        }

        return secondHighest;
    }

}
