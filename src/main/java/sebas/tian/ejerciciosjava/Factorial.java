package sebas.tian.ejerciciosjava;

/**
 *
 * @author jvelez
 */
public class Factorial {

    /**
     * calculates the factorial of N.
     *
     * @param n
     * @return
     */
    public int factorial(int n) {
        
        if (n > 1) { 

            // This is the recursive case
            return factorial(n - 1) * n;
        } else { 
            
            // Base case
            return 1;
        }
    }
}
